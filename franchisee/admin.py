from django.contrib import admin

from .models import (
    Category,
    Formats,
    Franchisee,
    Interest,
    SubCategory
)

admin.site.register(Category)
admin.site.register(Formats)
admin.site.register(Franchisee)
admin.site.register(Interest)
admin.site.register(SubCategory)
