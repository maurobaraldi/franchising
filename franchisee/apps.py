from django.apps import AppConfig


class FranchiseeConfig(AppConfig):
    name = 'franchisee'
