# Generated by Django 2.1.3 on 2019-08-05 01:34

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('franchisee', '0002_auto_20190805_0131'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Interests',
            new_name='Interest',
        ),
    ]
