import datetime

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.timezone import now

def current_year():
    return datetime.date.today().year


def max_value_current_year(value):
    return MaxValueValidator(current_year())(value)


class Formats(models.Model):
    name = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')


class Interest(models.Model):
    name = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')


class Category(models.Model):
    name = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')


class SubCategory(models.Model):
    name = models.CharField(max_length=200)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    pub_date = models.DateTimeField('date published')


class Franchisee(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    funding = models.IntegerField(validators=[MinValueValidator(1984), max_value_current_year], default=now)
    franchising = models.IntegerField(validators=[MinValueValidator(1984), max_value_current_year], default=now)
    executive = models.CharField(max_length=200, blank=True, null=True)
    formats = models.ManyToManyField(Formats)
    own_stores = models.IntegerField(default=0)
    franchisees = models.IntegerField(default=0)
    interests = models.ManyToManyField(Interest)
