from franchisee.models import Category
from rest_framework import viewsets
from franchisee.serializers import CategorySerializer


class CategoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows categories to be viewed or edited.
    """
    queryset = Category.objects.all().order_by('-pub_date')
    serializer_class = CategorySerializer
